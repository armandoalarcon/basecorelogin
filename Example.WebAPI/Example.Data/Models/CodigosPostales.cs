﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("CodigosPostales", Schema = "Sepomex")]
    public partial class CodigosPostales
    {
        public CodigosPostales()
        {
            Asentamientos = new HashSet<Asentamientos>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(5)]
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        public int MunicipioId { get; set; }

        [ForeignKey("MunicipioId")]
        [InverseProperty("CodigosPostales")]
        public Municipios Municipio { get; set; }
        [InverseProperty("CodigoPostal")]
        public ICollection<Asentamientos> Asentamientos { get; set; }
    }
}
