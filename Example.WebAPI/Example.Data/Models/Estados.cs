﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Estados", Schema = "Sepomex")]
    public partial class Estados
    {
        public Estados()
        {
            Municipios = new HashSet<Municipios>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(2)]
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }

        [InverseProperty("Estado")]
        public ICollection<Municipios> Municipios { get; set; }
    }
}
