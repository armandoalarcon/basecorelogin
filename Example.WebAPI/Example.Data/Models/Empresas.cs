﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Empresas", Schema = "Jerarquia")]
    public partial class Empresas
    {
        public Empresas()
        {
            Clientes = new HashSet<Clientes>();
            Colaboraciones = new HashSet<Colaboraciones>();
            EtapasCicloDeVida = new HashSet<EtapasCicloDeVida>();
            Regiones = new HashSet<Regiones>();
            Tamanios = new HashSet<Tamanios>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(3)]
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string RazonSocial { get; set; }
        [Required]
        [StringLength(100)]
        public string Alias { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }

        [InverseProperty("Empresa")]
        public ICollection<Clientes> Clientes { get; set; }
        [InverseProperty("Empresa")]
        public ICollection<Colaboraciones> Colaboraciones { get; set; }
        [InverseProperty("Empresa")]
        public ICollection<EtapasCicloDeVida> EtapasCicloDeVida { get; set; }
        [InverseProperty("Empresa")]
        public ICollection<Regiones> Regiones { get; set; }
        [InverseProperty("Empresa")]
        public ICollection<Tamanios> Tamanios { get; set; }
    }
}
