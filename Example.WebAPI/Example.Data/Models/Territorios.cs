﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Territorios", Schema = "Jerarquia")]
    public partial class Territorios
    {
        public Territorios()
        {
            Cedis = new HashSet<Cedis>();
            Desarrolladores = new HashSet<Desarrolladores>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(100)]
        public string Alias { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        public int RegionId { get; set; }

        [ForeignKey("RegionId")]
        [InverseProperty("Territorios")]
        public Regiones Region { get; set; }
        [InverseProperty("Territorio")]
        public ICollection<Cedis> Cedis { get; set; }
        [InverseProperty("Territorio")]
        public ICollection<Desarrolladores> Desarrolladores { get; set; }
    }
}
