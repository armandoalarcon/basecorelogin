﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("TiposDireccion", Schema = "Cliente")]
    public partial class TiposDireccion
    {
        public TiposDireccion()
        {
            Direcciones = new HashSet<Direcciones>();
        }

        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Descripcion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaIngreso { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }

        [InverseProperty("TipoDireccion")]
        public ICollection<Direcciones> Direcciones { get; set; }
    }
}
