﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Example.Data.Models.Servicios.Comun
{
    public interface IControl
    {
        string UsuarioIdAlta { get; set; }
        DateTime FechaAlta { get; set; }
        string UsuarioIdModificacion { get; set; }
        DateTime? FechaModificacion { get; set; }
        string UsuarioIdBaja { get; set; }
        DateTime? FechaBaja { get; set; }
    }
}
