﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Example.Data.Models.Servicios.Comun
{
    public interface IMarca : IControl
    {
        int Id { get; set; }
        string Codigo { get; set; }
        string Nombre { get; set; }
        string Descripcion { get; set; }
        int Estatus { get; set; }
    }
}
