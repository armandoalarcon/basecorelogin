﻿using Example.Data.Models.Servicios.Comun;
using System;
using System.Collections.Generic;
using System.Text;

namespace Example.Data.Models.Servicios.Beneficio
{
    public interface ICredencialRecarga : IControl
    {
        int Id { get; set; }
        string Codigo { get; set; }
        string Nombre { get; set; }
        string Descripcion { get; set; }
        int CadenaId { get; set; }
        int EstablecimientoId { get; set; }
        string Cajero { get; set; }
        string Contrasenia { get; set; }
        string Terminal { get; set; }
        string Wsdl { get; set; }
        int Estatus { get; set; }

        int AmbienteId { get; set; }
    }
}
