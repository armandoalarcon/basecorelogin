﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Regiones", Schema = "Jerarquia")]
    public partial class Regiones
    {
        public Regiones()
        {
            Territorios = new HashSet<Territorios>();
        }

        public int Id { get; set; }
        public int Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(100)]
        public string Alias { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        public int EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("Regiones")]
        public Empresas Empresa { get; set; }
        [InverseProperty("Region")]
        public ICollection<Territorios> Territorios { get; set; }
    }
}
