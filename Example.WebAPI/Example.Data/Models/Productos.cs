﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Productos", Schema = "Producto")]
    public partial class Productos
    {
        public int Id { get; set; }
        [Required]
        [StringLength(18)]
        public string Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(50)]
        public string CodigoBarras { get; set; }
        public int? UnidadMedidaId { get; set; }
        public int? MarcaId { get; set; }
        public int? SegmentoId { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }

        [ForeignKey("MarcaId")]
        [InverseProperty("Productos")]
        public Marcas Marca { get; set; }
        [ForeignKey("SegmentoId")]
        [InverseProperty("Productos")]
        public Segmentos Segmento { get; set; }
        [ForeignKey("UnidadMedidaId")]
        [InverseProperty("Productos")]
        public UnidadesMedida UnidadMedida { get; set; }
    }
}
