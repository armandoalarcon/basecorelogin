﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("BeneficiosPaquetes", Schema = "Configuracion")]
    public partial class BeneficiosPaquetes
    {
        public int BeneficioId { get; set; }
        public int PaqueteId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaInicio { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaFin { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
    }
}
