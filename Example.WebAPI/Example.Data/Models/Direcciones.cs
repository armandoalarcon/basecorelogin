﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Direcciones", Schema = "Cliente")]
    public partial class Direcciones
    {
        public int Id { get; set; }
        public int TipoDireccionId { get; set; }
        public int Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string RazonSocial { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaIngreso { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        public int ClienteId { get; set; }

        [ForeignKey("ClienteId")]
        [InverseProperty("Direcciones")]
        public Clientes Cliente { get; set; }
        [ForeignKey("TipoDireccionId")]
        [InverseProperty("Direcciones")]
        public TiposDireccion TipoDireccion { get; set; }
    }
}
