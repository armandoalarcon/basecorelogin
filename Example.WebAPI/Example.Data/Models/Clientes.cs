﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Example.Data.Models
{
    [Table("Clientes", Schema = "Cliente")]
    public partial class Clientes
    {
        public Clientes()
        {
            Direcciones = new HashSet<Direcciones>();
        }

        public int Id { get; set; }
        public int Codigo { get; set; }
        [Required]
        [StringLength(100)]
        public string RazonSocial { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaIngreso { get; set; }
        public int Estatus { get; set; }
        public int UsuarioIdAlta { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }
        public int? UsuarioIdModificacion { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }
        public int? UsuarioIdBaja { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        public int EmpresaId { get; set; }

        [ForeignKey("EmpresaId")]
        [InverseProperty("Clientes")]
        public Empresas Empresa { get; set; }
        [InverseProperty("Cliente")]
        public ICollection<Direcciones> Direcciones { get; set; }
    }
}
