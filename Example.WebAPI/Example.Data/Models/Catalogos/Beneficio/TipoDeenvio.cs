﻿using Example.Data.Models.Servicios.Beneficio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Example.Data.Models.Catalogos.Beneficio
{
    [Table("TiposDeEnvio", Schema = "Beneficio")] /* LMS_MC_DiagProPortalWeb en Sistemas/Guias */
    public class TipoDeEnvio : ITipoDeEnvio
    {
        #region   < < <   C o n s t r u c t o r e s   > > >
        public TipoDeEnvio()
        {
            Premios = new HashSet<Premio>();
        }
        #endregion

        #region   < < <   L l a v e   p r i m a r i a   > > >
        [Key]
        [Column(TypeName = "int")]
        public int Id { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
        [Required]
        [StringLength(6)]
        [Column(TypeName = "nvarchar(6)")]  /* LEVPED -> Lavevantamiento de pedido, REPSEM -> Envío de reporte de Ike..., */
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Descripcion { get; set; }

        [Required]
        [Column(TypeName = "int")]
        [Range(0, 2)]
        public int Estatus { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   d e   c o n t r o l   > > >
        [Required]
        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdAlta { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdModificacion { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdBaja { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        #endregion

        #region   < < <   D e p e n d e n c i a s   > > >

        #endregion

        #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
        #region   < < <   P a d r e s   > > >

        #endregion

        #region   < < <   H i j o s   > > >
        [InverseProperty("TiposDeCanje")]
        public ICollection<Premio> Premios { get; set; }
        #endregion
        #endregion
    }
}
