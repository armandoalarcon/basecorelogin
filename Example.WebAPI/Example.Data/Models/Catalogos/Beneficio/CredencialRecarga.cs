﻿using Example.Data.Models.Servicios.Beneficio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Example.Data.Models.Catalogos.Beneficio
{
    [Table("CredencialesRecarga", Schema = "Beneficio")]
    public class CredencialRecarga : ICredencialRecarga
    {
        #region   < < <   C o n s t r u c t o r e s   > > >
        public CredencialRecarga()
        {
            Carriers = new HashSet<Carrier>();
        }
        #endregion

        #region   < < <   L l a v e   p r i m a r i a   > > >
        [Key]
        [Column(TypeName = "int")]
        public int Id { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
        [Required]
        [StringLength(4)]
        [Column(TypeName = "nvarchar(4)")]  /* CRED -> Credenta, PRUE -> Prueba */
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Descripcion { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int CadenaId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int EstablecimientoId { get; set; }

        [Required]
        [StringLength(50)]
        [Column(TypeName = "nvarchar(50)")]
        public string Cajero { get; set; }

        [Required]
        [StringLength(50)]
        [Column(TypeName = "nvarchar(50)")]
        public string Contrasenia { get; set; }

        [Required]
        [StringLength(50)]
        [Column(TypeName = "nvarchar(50)")]
        public string Terminal { get; set; }

        [Required]
        [StringLength(250)]
        [Column(TypeName = "nvarchar(250)")]
        public string Wsdl { get; set; }

        [Required]
        [Column(TypeName = "int")]
        [Range(0, 2)]
        public int Estatus { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   d e   c o n t r o l   > > >
        [Required]
        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdAlta { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdModificacion { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdBaja { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        #endregion

        #region   < < <   D e p e n d e n c i a s   > > >
        [Required]
        [Column(TypeName = "int")]
        public int AmbienteId { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
        #region   < < <   P a d r e s   > > >
        [ForeignKey("AmbienteId")]
        [InverseProperty("CredencialesRecarga")]
        public Ambiente Ambientes { get; set; }
        #endregion

        #region   < < <   H i j o s   > > >
        [InverseProperty("CredencialesRecarga")]
        public ICollection<Carrier> Carriers { get; set; }
        #endregion
        #endregion
    }
}