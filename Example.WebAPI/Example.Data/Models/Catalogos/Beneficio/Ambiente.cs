﻿using Example.Data.Models.Servicios.Comun;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Example.Data.Models.Catalogos.Beneficio
{
    [Table("Ambientes", Schema = "Beneficio")]
    public class Ambiente : IAmbiente
    {
        #region   < < <   C o n s t r u c t o r e s   > > >
        public Ambiente()
        {
            CredencialesRecarga = new HashSet<CredencialRecarga>();
        }
        #endregion

        #region   < < <   L l a v e   p r i m a r i a   > > >
        [Key]
        [Column(TypeName = "int")]
        public int Id { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
        [Required]
        [StringLength(1)]
        [Column(TypeName = "nvarchar(1)")]  /* 0 -> Desarrollo, 1 -> Producción */
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Descripcion { get; set; }

        [Required]
        [Column(TypeName = "int")]
        [Range(0, 2)]
        public int Estatus { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   d e   c o n t r o l   > > >
        [Required]
        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdAlta { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdModificacion { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdBaja { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        #endregion

        #region   < < <   D e p e n d e n c i a s   > > >

        #endregion

        #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
        #region   < < <   P a d r e s   > > >

        #endregion

        #region   < < <   H i j o s   > > >
        [InverseProperty("Ambientes")]
        public ICollection<CredencialRecarga> CredencialesRecarga { get; set; }
        #endregion
        #endregion
    }
}
