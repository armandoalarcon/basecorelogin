﻿using Example.Data.Models.Servicios.Beneficio;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Example.Data.Models.Catalogos.Beneficio
{
    [Table("Premios", Schema = "Beneficio")]
    public class Premio : IPremio
    {
        #region   < < <   C o n s t r u c t o r e s   > > >
        public Premio()
        {
        }
        #endregion

        #region   < < <   L l a v e   p r i m a r i a   > > >
        [Key]
        [Column(TypeName = "int")]
        public int Id { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   n a t u r a l e s   > > >
        [Required]
        [StringLength(20)]
        [Column(TypeName = "nvarchar(20)")]
        public string Codigo { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Nombre { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string NombreCorto { get; set; }

        [Required]
        [StringLength(100)]
        [Column(TypeName = "nvarchar(100)")]
        public string Descripcion { get; set; }

        [Required]
        [Column(TypeName = "int")]
        [Range(0, 2)]
        public int Estatus { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   d e   c o n t r o l   > > >
        [Required]
        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdAlta { get; set; }

        [Required]
        [Column(TypeName = "datetime")]
        public DateTime FechaAlta { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdModificacion { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaModificacion { get; set; }

        [StringLength(256)]
        [Column(TypeName = "nvarchar(256)")]
        public string UsuarioIdBaja { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime? FechaBaja { get; set; }
        #endregion

        #region   < < <   D e p e n d e n c i a s   > > >
        [Required]
        [Column(TypeName = "int")]
        public int MarcaId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int ProveedorId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int CategoriaId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int ClasificacionId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int TipoDeCanjeId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int CarrierId { get; set; }

        [Required]
        [Column(TypeName = "int")]
        public int TipoDeEnvio { get; set; }
        #endregion

        #region   < < <   P r o p i e d a d e s   d e   n a v e g a c i ó n   > > >
        #region   < < <   P a d r e s   > > >
        [ForeignKey("MarcaPremioId")]
        [InverseProperty("Premios")]
        public MarcaPremio MarcasPremio { get; set; }

        [ForeignKey("ProveedorId")]
        [InverseProperty("Premios")]
        public Proveedor Proveedores { get; set; }

        [ForeignKey("CategoriaId")]
        [InverseProperty("Premios")]
        public Categoria Categorias { get; set; }

        [ForeignKey("ClasificacionId")]
        [InverseProperty("Premios")]
        public Clasificacion Clasificaciones { get; set; }

        [ForeignKey("TipoDeCanjeId")]
        [InverseProperty("Premios")]
        public TipoDeCanje TiposDeCanje { get; set; }

        [ForeignKey("CarrierId")]
        [InverseProperty("Premios")]
        public Carrier Carriers { get; set; }

        [ForeignKey("TipoDeEnvioId")]
        [InverseProperty("Premios")]
        public TipoDeEnvio TiposDeEnvio { get; set; }
        #endregion

        #region   < < <   H i j o s   > > >

        #endregion
        #endregion
    }
}
