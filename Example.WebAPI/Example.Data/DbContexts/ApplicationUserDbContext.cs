﻿using Example.Data.Entities;
using Example.Data.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Example.Data.DbContexts
{
    //Por verificar
    //public class ApplicationUserDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>,ApplicationUserRole, IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>
    public class ApplicationUserDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationUserDbContext(DbContextOptions<ApplicationUserDbContext> options) : base(options)
        {

        }

        public string GetUserId()
        {
            HttpContextAccessor _context = new HttpContextAccessor();
            return _context.HttpContext.User?.Claims?.Where(c => c.Type == ClaimTypes.NameIdentifier)
                                                             .Select(c => c.Value).SingleOrDefault();
        }

        public override Task<int> SaveChangesAsync(System.Threading.CancellationToken cancellationToken = default(System.Threading.CancellationToken))
        {
            try
            {
                var now = DateTime.Now;
                var userId = GetUserId();
                foreach (var entry in ChangeTracker.Entries())
                {
                    var propertyNames = entry.Metadata.GetProperties().Select(p => p.Name);
                    if (entry.State == EntityState.Added)
                    {
                        //entry.Property("UsuarioIdAlta").CurrentValue = userId;
                        if (propertyNames.Contains("UsuarioIdAlta"))
                            entry.Property("UsuarioIdAlta").CurrentValue = 1;
                        //entry.Property("FechaAlta").CurrentValue = now;
                    }
                    else if (entry.State == EntityState.Modified) // If the entity was updated
                    {
                        //entry.Property("UsuarioIdModificacion").CurrentValue = userId;
                        if (propertyNames.Contains("UsuarioIdModificacion"))
                            entry.Property("UsuarioIdModificacion").CurrentValue = 1;
                        if (propertyNames.Contains("FechaModificacion"))
                            entry.Property("FechaModificacion").CurrentValue = now;
                    }
                }
                return base.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }
        //Por verificar
        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    base.OnModelCreating(builder);

        //    builder.Entity<ApplicationUserRole>(userRole =>
        //    {
        //        userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

        //        userRole.HasOne(ur => ur.Role)
        //            .WithMany(r => r.UserRoles)
        //            .HasForeignKey(ur => ur.RoleId)
        //            .IsRequired();

        //        userRole.HasOne(ur => ur.User)
        //            .WithMany(r => r.UserRoles)
        //            .HasForeignKey(ur => ur.UserId)
        //            .IsRequired();
        //    });
        //}

        public virtual DbSet<Anios> Anios { get; set; }
        public virtual DbSet<Asentamientos> Asentamientos { get; set; }
        //public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        //public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        //public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        //public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        //public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Beneficios> Beneficios { get; set; }
        public virtual DbSet<BeneficiosPaquetes> BeneficiosPaquetes { get; set; }
        public virtual DbSet<Bimestres> Bimestres { get; set; }
        public virtual DbSet<Campanias> Campanias { get; set; }
        public virtual DbSet<CampaniasBeneficios> CampaniasBeneficios { get; set; }
        public virtual DbSet<Cedis> Cedis { get; set; }
        public virtual DbSet<Clientes> Clientes { get; set; }
        public virtual DbSet<CodigosPostales> CodigosPostales { get; set; }
        public virtual DbSet<Colaboraciones> Colaboraciones { get; set; }
        public virtual DbSet<Desarrolladores> Desarrolladores { get; set; }
        public virtual DbSet<Dias> Dias { get; set; }
        public virtual DbSet<Direcciones> Direcciones { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<Estados> Estados { get; set; }
        public virtual DbSet<EtapasCicloDeVida> EtapasCicloDeVida { get; set; }
        public virtual DbSet<Marcas> Marcas { get; set; }
        public virtual DbSet<Meses> Meses { get; set; }
        public virtual DbSet<Municipios> Municipios { get; set; }
        public virtual DbSet<Paquetes> Paquetes { get; set; }
        public virtual DbSet<PaquetesArticulos> PaquetesArticulos { get; set; }
        public virtual DbSet<Preventistas> Preventistas { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<Programas> Programas { get; set; }
        public virtual DbSet<ProgramasCampanias> ProgramasCampanias { get; set; }
        public virtual DbSet<Quincenas> Quincenas { get; set; }
        public virtual DbSet<Regiones> Regiones { get; set; }
        public virtual DbSet<Segmentos> Segmentos { get; set; }
        public virtual DbSet<Semanas> Semanas { get; set; }
        public virtual DbSet<Semestres> Semestres { get; set; }
        public virtual DbSet<Tamanios> Tamanios { get; set; }
        public virtual DbSet<Territorios> Territorios { get; set; }
        public virtual DbSet<TiposDireccion> TiposDireccion { get; set; }
        public virtual DbSet<Trimestres> Trimestres { get; set; }
        public virtual DbSet<UnidadesMedida> UnidadesMedida { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=184.107.55.173;Initial Catalog=AC_V2M;User Id=ac_v2m;Password=arca*2019");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Anios>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Anios_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Asentamientos>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Sepomex_Asentamientos_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.CodigoPostal)
                    .WithMany(p => p.Asentamientos)
                    .HasForeignKey(d => d.CodigoPostalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Sepomex_Asentamientos_CodigosPostales");
            });

            //modelBuilder.Entity<AspNetRoleClaims>(entity =>
            //{
            //    entity.HasIndex(e => e.RoleId);
            //});

            //modelBuilder.Entity<AspNetRoles>(entity =>
            //{
            //    entity.HasIndex(e => e.NormalizedName)
            //        .HasName("RoleNameIndex")
            //        .IsUnique()
            //        .HasFilter("([NormalizedName] IS NOT NULL)");

            //    entity.Property(e => e.Id).ValueGeneratedNever();
            //});

            //modelBuilder.Entity<AspNetUserClaims>(entity =>
            //{
            //    entity.HasIndex(e => e.UserId);
            //});

            //modelBuilder.Entity<AspNetUserLogins>(entity =>
            //{
            //    entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

            //    entity.HasIndex(e => e.UserId);
            //});

            //modelBuilder.Entity<AspNetUserRoles>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.RoleId });

            //    entity.HasIndex(e => e.RoleId);
            //});

            //modelBuilder.Entity<AspNetUsers>(entity =>
            //{
            //    entity.HasIndex(e => e.NormalizedEmail)
            //        .HasName("EmailIndex");

            //    entity.HasIndex(e => e.NormalizedUserName)
            //        .HasName("UserNameIndex")
            //        .IsUnique()
            //        .HasFilter("([NormalizedUserName] IS NOT NULL)");

            //    entity.Property(e => e.Id).ValueGeneratedNever();
            //});

            //modelBuilder.Entity<AspNetUserTokens>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });
            //});

            modelBuilder.Entity<Beneficios>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Configuracion_Beneficios_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<BeneficiosPaquetes>(entity =>
            {
                entity.HasKey(e => new { e.BeneficioId, e.PaqueteId });

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Bimestres>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Bimestres_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Campanias>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Configuracion_Campanias_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<CampaniasBeneficios>(entity =>
            {
                entity.HasKey(e => new { e.CampaniaId, e.BeneficioId });

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Cedis>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Jerarquia_Cedis_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Territorio)
                    .WithMany(p => p.Cedis)
                    .HasForeignKey(d => d.TerritorioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Jerarquia_Cedis_Territorios");
            });

            modelBuilder.Entity<Clientes>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Cliente_Clientes_Codigo")
                    .IsUnique();

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RazonSocial).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.EmpresaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Cliente_Jerarquia_Empresas");
            });

            modelBuilder.Entity<CodigosPostales>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Sepomex_CodigosPostales_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Municipio)
                    .WithMany(p => p.CodigosPostales)
                    .HasForeignKey(d => d.MunicipioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Sepomex_CodigosPostales_Municipios");
            });

            modelBuilder.Entity<Colaboraciones>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Segmentacion_Colaboraciones_Codigo")
                    .IsUnique();

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Colaboraciones)
                    .HasForeignKey(d => d.EmpresaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Segmentacion_Colaboraciones_Empresas");
            });

            modelBuilder.Entity<Desarrolladores>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_FuerzaVenta_Desarrolladores_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Territorio)
                    .WithMany(p => p.Desarrolladores)
                    .HasForeignKey(d => d.TerritorioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_FuerzaVenta_Desarrolladores_Territorios");
            });

            modelBuilder.Entity<Dias>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Dias_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Direcciones>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Cliente_Direcciones_Codigo")
                    .IsUnique();

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RazonSocial).IsUnicode(false);

                entity.HasOne(d => d.Cliente)
                    .WithMany(p => p.Direcciones)
                    .HasForeignKey(d => d.ClienteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Cliente_Direcciones_Clientes");

                entity.HasOne(d => d.TipoDireccion)
                    .WithMany(p => p.Direcciones)
                    .HasForeignKey(d => d.TipoDireccionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Cliente_Direcciones_TiposDireccion");
            });

            modelBuilder.Entity<Empresas>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Jerarquia_Empresas_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.RazonSocial).IsUnicode(false);
            });

            modelBuilder.Entity<Estados>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Sepomex_Estados_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<EtapasCicloDeVida>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Segmentacion_EtapasCicloDeVida_Codigo")
                    .IsUnique();

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.EtapasCicloDeVida)
                    .HasForeignKey(d => d.EmpresaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Segmentacion_EtapasCicloDeVida_Empresas");
            });

            modelBuilder.Entity<Marcas>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Producto_Marcas_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Meses>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Meses_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Municipios>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Sepomex_Municipios_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Estado)
                    .WithMany(p => p.Municipios)
                    .HasForeignKey(d => d.EstadoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Sepomex_Municipios_Estados");
            });

            modelBuilder.Entity<Paquetes>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Configuracion_Paquetes_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<PaquetesArticulos>(entity =>
            {
                entity.HasKey(e => new { e.PaqueteId, e.ArticuloId });

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Preventistas>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_FuerzaVenta_Preventistas_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Desarrollador)
                    .WithMany(p => p.Preventistas)
                    .HasForeignKey(d => d.DesarrolladorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_FuerzaVenta_Preventistas_Desarrolladores");
            });

            modelBuilder.Entity<Productos>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Producto_Productos_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.CodigoBarras).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Marca)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.MarcaId)
                    .HasConstraintName("fk_Producto_Productos_Marcas");

                entity.HasOne(d => d.Segmento)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.SegmentoId)
                    .HasConstraintName("fk_Producto_Productos_Segmentos");

                entity.HasOne(d => d.UnidadMedida)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.UnidadMedidaId)
                    .HasConstraintName("fk_Producto_Productos_UnidadesMedida");
            });

            modelBuilder.Entity<Programas>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Configuracion_Programas_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<ProgramasCampanias>(entity =>
            {
                entity.HasKey(e => new { e.ProgramaId, e.CampaniaId });

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Quincenas>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Quincenas_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Regiones>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Jerarquia_Regiones_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Regiones)
                    .HasForeignKey(d => d.EmpresaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Jerarquia_Regiones_Empresas");
            });

            modelBuilder.Entity<Segmentos>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Producto_Segmentos_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Semanas>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Semanas_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Semestres>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Semestres_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<Tamanios>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Segmentacion_Tamanios_Codigo")
                    .IsUnique();

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Empresa)
                    .WithMany(p => p.Tamanios)
                    .HasForeignKey(d => d.EmpresaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Segmentacion_Tamanios_Empresas");
            });

            modelBuilder.Entity<Territorios>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Jerarquia_Territorios_Codigo")
                    .IsUnique();

                entity.Property(e => e.Alias).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Territorios)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Jerarquia_Territorios_Regiones");
            });

            modelBuilder.Entity<TiposDireccion>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Cliente_TiposDireccion_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Descripcion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Trimestres>(entity =>
            {
                entity.HasIndex(e => e.Abreviacion)
                    .HasName("ix_Tiempo_Trimestres_Abreviacion")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Abreviacion).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });

            modelBuilder.Entity<UnidadesMedida>(entity =>
            {
                entity.HasIndex(e => e.Codigo)
                    .HasName("ix_Producto_UnidadesMedida_Codigo")
                    .IsUnique();

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Estatus).HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaAlta).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Nombre).IsUnicode(false);
            });
        }
    }
}
