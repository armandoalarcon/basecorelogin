﻿using Example.Data.Entities;
using Example.Data.Models.Catalogos.Beneficio;
using Example.Data.Models.Catalogos.Cliente;
using Example.Data.Models.Catalogos.FuerzaDeVenta;
using Example.Data.Models.Catalogos.Jerarquia;
using Example.Data.Models.Catalogos.Presupuesto;
using Example.Data.Models.Catalogos.Producto;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Example.Data.Models.Catalogos.Segmentacion;
using Example.Data.Models.Catalogos.Sepomex;
using Example.Data.Models.Catalogos.Tiempo;
using Example.Data.Models.Catalogos.TipoDeInversion;

namespace Example.Data.DbContexts
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        #region   < < <   D e f i n i c i ó n   d e   l o s   c o n j u n t o s   d e   d a t o s   > > >
        #region   < < <   E s q u e m a   B e n e f i c i o   > > >
        public DbSet<Carrier> Carriers { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Clasificacion> Clasificaciones { get; set; }
        public DbSet<MarcaPremio> MarcasPremio { get; set; }
        public DbSet<Premio> Premios { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }
        public DbSet<TipoDeCanje> TiposDeCanje { get; set; }
        public DbSet<TipoDeEnvio> TiposDeEnvio { get; set; }
        #endregion
        #region   < < <   E s q u e m a   C l i e n t e   > > >
        public DbSet<Canal> Canales { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }
        public DbSet<Propietario> Propietarios { get; set; }
        public DbSet<Subcanal> Subcanales { get; set; }
        public DbSet<TipoDeDireccion> TiposDeDireccion { get; set; }

        #endregion
        #region   < < <   E s q u e m a   F u e r z a D e V e n t a   > > >
        public DbSet<Coordinador> Coordinadores { get; set; }
        public DbSet<Desarrollador> Desarrolladores { get; set; }
        public DbSet<JefeDeMercado> JefesDeMercado { get; set; }
        public DbSet<Preventista> Preventistas { get; set; }
        #endregion
        #region   < < <   E s q u e m a   J e r a r q u i a   > > >
        public DbSet<CentroDeDistribucion> CentrosDeDistribucion { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Region> Regiones { get; set; }
        public DbSet<Territorio> Territorios { get; set; }
        #endregion
        #region   < < <   E s q u e m a   P r e s u p u e s t o   > > >
        public DbSet<TipoDePresupuesto> TiposDePresupuesto { get; set; }
        #endregion
        #region   < < <   E s q u e m a   P r o d u c t o   > > >
        public DbSet<Contenido> Contenidos { get; set; }
        public DbSet<DirigidoA> DirigidosA { get; set; }
        public DbSet<MarcaProducto> MarcasProducto { get; set; }
        public DbSet<Material> Materiales { get; set; }
        public DbSet<Presentacion> Presentaciones { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Sabor> Sabores { get; set; }
        public DbSet<Segmento> Segmentos { get; set; }
        #endregion
        #region   < < <   E s q u e m a   S e g m e n t a c i o n   > > >
        public DbSet<EtapaDeCicloDeVida> EtapasDeCicloDeVida { get; set; }
        public DbSet<Nivel> Niveles { get; set; }
        public DbSet<NivelDeColaboracion> NivelesDeColaboracion { get; set; }
        public DbSet<Tamanio> Tamanios { get; set; }
        #endregion
        #region   < < <   E s q u e m a   S e p o m e x   > > >
        public DbSet<CodigoPostal> CodigosPostales { get; set; }
        public DbSet<Colonia> Colonias { get; set; }
        public DbSet<Estado> Estados { get; set; }
        public DbSet<Municipio> Municipios { get; set; }
        public DbSet<Pais> Paises { get; set; }
        public DbSet<Poblacion> Poblaciones { get; set; }
        #endregion
        #region   < < <   E s q u e m a   T i e m p o   > > >
        public DbSet<Anio> Anios { get; set; }
        public DbSet<Bimestre> Bimestres { get; set; }
        public DbSet<Cuatrimestre> Cuatrimestres { get; set; }
        public DbSet<Dia> Dias { get; set; }
        public DbSet<Mes> Meses { get; set; }
        public DbSet<Quincena> Quincenas { get; set; }
        public DbSet<Semana> Semanas { get; set; }
        public DbSet<Semestre> Semestres { get; set; }
        public DbSet<Trimestre> Trimestres { get; set; }
        #endregion
        #region   < < <   E s q u e m a   T i p o D e I n v e r s i o n   > > >
        public DbSet<GrupoDeTipoDeInversion> GruposDeTipoDeInversion { get; set; }
        public DbSet<TipoDeInversion> TiposDeInversion { get; set; }
        #endregion
        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=184.107.55.173;Initial Catalog=AC_V2M;User Id=ac_v2m;Password=arca*2019");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region   < < <   C o n f i g u r a c i ó n   d e   l o s   c o n j u n t o s   d e   d a t o s   > > >
            #region   < < <   E s q u e m a   B e n e f i c i o   > > >
            //modelBuilder.Entity<Carrier>({ 
                
            //});
            #endregion

            #region   < < <   E s q u e m a   T i e m p o   > > >
            modelBuilder.Entity<Anio>(e => {
                e.HasIndex(p => p.Codigo).HasName("ix_Tiempo_Anios_Codigo").IsUnique();
                e.Property(p => p.Codigo).IsUnicode(false);
                e.Property(p => p.Nombre).IsUnicode(false);
                e.Property(p => p.Estatus).HasDefaultValueSql("((1))");
                e.Property(p => p.FechaAlta).HasDefaultValueSql("(getdate())");
                e.Property(p => p.UsuarioIdAlta).IsUnicode(false);
                e.Property(p => p.UsuarioIdModificacion).IsUnicode(false);
                e.Property(p => p.UsuarioIdBaja).IsUnicode(false);
            });
            #endregion
            #endregion
        }
    }
}
