﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Example.Data.Entities
{
    public class ApplicationUser: IdentityUser
    {
        [Required]
        [MaxLength(200)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(250)]
        public string LastName { get; set; }

        //Por verificar
        //public ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
