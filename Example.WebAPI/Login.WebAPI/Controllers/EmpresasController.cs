﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Example.Data.DbContexts;
using Example.Data.Models;
using Microsoft.Extensions.Logging;

namespace Login.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpresasController : ControllerBase
    {
        private readonly ApplicationUserDbContext _context;
        private readonly ILogger<EmpresasController> logger;

        public EmpresasController(ApplicationUserDbContext context, ILogger<EmpresasController> _logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empresas>>> GetEmpresas()
        {
            try
            {
                return await _context.Empresas.ToListAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Empresas>> GetEmpresas(int id)
        {
            try
            {
                var empresas = await _context.Empresas.FindAsync(id);

                if (empresas == null)
                {
                    return NotFound();
                }

                return empresas;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpresas(int id, Empresas empresas)
        {
            if (id != empresas.Id)
            {
                return BadRequest();
            }

            _context.Entry(empresas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                logger.LogError(ex.Message);
                if (!EmpresasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500);
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Empresas>> PostEmpresas(Empresas empresas)
        {
            try
            {
                _context.Empresas.Add(empresas);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetEmpresas", new { id = empresas.Id }, empresas);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Empresas>> DeleteEmpresas(int id)
        {
            try
            {
                var empresas = await _context.Empresas.FindAsync(id);
                if (empresas == null)
                {
                    return NotFound();
                }

                _context.Empresas.Remove(empresas);
                await _context.SaveChangesAsync();

                return empresas;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }

        #region Helpers

        private bool EmpresasExists(int id)
        {
            return _context.Empresas.Any(e => e.Id == id);
        }

        #endregion

    }
}
