﻿using Example.Data.DbContexts;
using Example.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Login.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TamaniosController : ControllerBase
    {
        private readonly ApplicationUserDbContext _context;
        private readonly ILogger<TamaniosController> logger;


        public TamaniosController(ApplicationUserDbContext context, ILogger<TamaniosController> _logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Tamanios>>> GetTamanios()
        {
            try
            {
                return await _context.Tamanios.ToListAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Tamanios>> GetTamanios(int id)
        {
            try
            {
                var tamanios = await _context.Tamanios.FindAsync(id);

                if (tamanios == null)
                {
                    return NotFound();
                }

                return tamanios;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutTamanios(int id, Tamanios tamanio)
        {
            if (id != tamanio.Id)
            {
                return BadRequest();
            }

            _context.Entry(tamanio).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                logger.LogError(ex.Message);
                if (!TamaniosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(500);
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Tamanios>> PostTamanios(Tamanios tamanio)
        {
            try
            {
                _context.Tamanios.Add(tamanio);
                tamanio.Estatus = 1;
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetTamanios", new { id = tamanio.Id }, tamanio);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Tamanios>> DeleteTamanios(int id)
        {
            try
            {
                var tamanio = await _context.Tamanios.FindAsync(id);
                if (tamanio == null)
                {
                    return NotFound();
                }

                _context.Tamanios.Remove(tamanio);
                await _context.SaveChangesAsync();

                return tamanio;
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return StatusCode(500);
            }

        }

        #region Helpers

        private bool TamaniosExists(int id)
        {
            return _context.Tamanios.Any(e => e.Id == id);
        }

        #endregion

    }
}
